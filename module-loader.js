(function(){
	'use strict';

	var parseName = function (name) {
        var index = name.indexOf("!");
        return name.substr(index+1);
    };

	define('mod', {
		version: '1.0.0',

		load: function(name, req, onload, config) {				
			var moduleName = parseName(name),
				moduleVersion = config.mods[moduleName];

			if (moduleVersion) {
                var uri = moduleName + '-' + moduleVersion;

				req([ uri ], function(value){
					req([moduleName + '/module'], function(module){
						onload(module);
					});
				});
			} else {
				console.error('module ' + moduleName + ' not found in config!');
			}			
		}
	});

}());